<?php

$user = [
    ['name' => 'kevin', 'age' => 15, 'skill' => 'C#'],
    ['name' => 'steve', 'age' => 28, 'skill' => 'php'],
    ['name' => 'jackie', 'age' => 40, 'skill' => 'javascript'],
    ['name' => 'louis', 'age' => 33, 'skill' => 'C#'],
    ['name' => 'jon', 'age' => 23, 'skill' => 'php'],
];

$data = [];
foreach ($user as $row) {
    if ($row['age'] > 20) {
        $data[$row['skill']][] = $row;
    }
}

print_r($data);
