<?php defined('BASEPATH') || exit('No direct script access allowed');

$db['default']['hostname'] = 'mysql';
$db['default']['username'] = 'root';
$db['default']['password'] = '1234';

$db['default']['database'] = 'newproject';
$db['default']['dbdriver'] = 'mysqli';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = false;
$db['default']['db_debug'] = true;
$db['default']['cache_on'] = false;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = true;
$db['default']['stricton'] = false;
$db['default']['table_pre'] = 'bc_'; //数据表的前缀
